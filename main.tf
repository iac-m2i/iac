terraform {
  required_providers {
    docker = { # Déclaration du fournisseur Docker requis
      source = "kreuzwerker/docker"
    }
  }
}

provider "docker" {} # Déclaration du fournisseur Docker

resource "null_resource" "create_directory"{
  provisioner "local-exec" {
    command = "mkdir -p /home/user/share"  # Création du répertoire /home/user/share
  }
}
resource "null_resource" "change_permissions" {
  provisioner "local-exec" {
    command = "chmod 777 /home/user/share"
  }
  triggers = {
    always_run = timestamp()  # Déclencheur pour exécuter cette ressource à chaque changement de timestamp
  }
}

resource "docker_container" "samba" {
  image = "dperson/samba"  # Utilisation de l'image Docker "dperson/samba"
  name  = "samba" # Nom du conteneur
  ports {
    internal = 139 # Port interne du conteneur
    external = 139 # Port externe du conteneur
  }

  ports {
    internal = 445 # Port interne du conteneur
    external = 445 # Port externe du conteneur
  }
  volumes {
    container_path  = "/mount/share"  # Chemin dans le conteneur pour le montage du volume
    host_path = "/home/user/share/"   # Chemin sur le système hôte à monter dans le conteneur
    read_only = false  # Volume monté en lecture-écriture
  }
  command = ["-s","share;/mount/share;yes;no;yes;all;none"] # Arguments passés au conteneur lors de son exécution
  
} 

